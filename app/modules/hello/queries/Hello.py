
from graphene import ObjectType, String
from graphql import GraphQLError

class Hello(ObjectType):
    hello = String()


    def resolve_hello(self, info):
             return 'Hello world'

from graphene import ObjectType

from .Hello import Hello


class HelloQuery(Hello, ObjectType):
    pass

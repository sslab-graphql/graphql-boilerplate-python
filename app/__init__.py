from config import Config

from flask import Flask, jsonify
from flask_graphql import GraphQLView
from flask_jwt_extended import JWTManager


def create_app():
    app = Flask(__name__)
    app.config.from_object((set_environment_config()))

    @app.route('/')
    def hello_world():
        return "Hello World"

    from app.schema import schema

    app.add_url_rule(
        '/graphql',
        view_func=GraphQLView.as_view(
            'graphql',
            schema=schema,
            graphiql=True  # for having the GraphiQL interface
        )
    )

    return app


def set_environment_config():
    if(Config.ENV == "PRODUCTION"):
        return 'config.ProductionConfig'
    elif (Config.ENV == "DEVELOPMENT"):
        return 'config.DevelopmentConfig'

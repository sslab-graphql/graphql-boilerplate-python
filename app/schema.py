from graphene import ObjectType, Schema, String

from app.modules.hello.queries import HelloQuery


class RootQuery(HelloQuery, ObjectType):
    pass


schema = Schema(query=RootQuery)

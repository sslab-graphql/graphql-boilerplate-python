import os


class Config(object):
    ENV = os.environ['ENV']
    CSRF_ENABLED = True

class DevelopmentConfig(Config):
    DEBUG = True


class ProductionConfig(Config):
    DEBUG = False
